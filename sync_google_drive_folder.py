#!/usr/bin/env python

"""


Created by Will Kearney
June 2018


"""

from __future__ import print_function

import os
import warnings
import sys

# google drive modules
from apiclient.discovery import build
from apiclient.http import MediaFileUpload
from apiclient import errors
from httplib2 import Http
from oauth2client import file, client, tools


from  Tkinter import Tk
import Tkinter, Tkconstants, tkFileDialog


# global vars
DRIVE_FOLDER_ID = '13lEPeanAnAcwmWEUQUxcZpp1QiEJwavb'
LOCAL_FOLDER_PATH = r"./PPS Dropbox Test"

client_secret_location = r"./client_secret.json"


def setup_drive_v3_api():
	# Setup the Drive v3 API
	SCOPES = 'https://www.googleapis.com/auth/drive'
	store = file.Storage('credentials.json')
	creds = store.get()
	if not creds or creds.invalid:
		flow = client.flow_from_clientsecrets(client_secret_location, SCOPES)
		creds = tools.run_flow(flow, store)
	service = build('drive', 'v3', http=creds.authorize(Http()))

	return service


def get_metadata_in_remote_folder(service, remote_folder_id):
	"""Returns the metadata of the subfolders located in folder_id"""
	query = "'{}' in parents and mimeType = 'application/vnd.google-apps.folder' and trashed = FALSE".format(remote_folder_id)

	try:
		response = service.files().list(q=query,
			spaces='drive',
			fields='files(id, name)').execute()
	except errors.HttpError, error:
		print('An error occurred: {}'.format(error))

	return response['files']


def choose_local_directory():
	"""Presents the user with a popup for directory selection."""
	Tk().withdraw()
	dir_path = tkFileDialog.askdirectory(title="Choose a directory to upload to Google Drive", initialdir=os.getcwd())
	return(dir_path)

def get_local_folder_metadata(local_folder_path):
	"""Returns the name of the directory as well as the list of subfolder names."""
	
	# get name of local directory
	local_dir_name = os.path.basename(local_folder_path)
	# print(local_dir_name)

	# get names of subfolders
	folder_names = os.listdir(local_folder_path)

	try:
		folder_names.remove('.DS_Store')
		print("Removed '.DS_Store' from local directory list.")
	except ValueError as e:
		pass

	# convert this to a dict so it's in the same format as the google drive info
	dir_list = []
	for folder_name in folder_names:
		unicode_folder_name = unicode(folder_name, "utf-8")
		dir_list.append( {u'name' : unicode_folder_name, u'path' : os.path.join(os.path.abspath(local_folder_path), folder_name)} )

	return local_dir_name, dir_list

# def get_metadata_in_local_folder(local_folder_path):
# 	folder_names = os.listdir(local_folder_path)

# 	try:
# 		folder_names.remove('.DS_Store')
# 		print("Removed '.DS_Store' from local directory list.")
# 	except ValueError as e:
# 		pass

# 	# convert this to a dict so it's in the same format as the google drive info
# 	dir_list = []
# 	for folder_name in folder_names:
# 		unicode_folder_name = unicode(folder_name, "utf-8")
# 		dir_list.append( {u'name' : unicode_folder_name} )

# 	return dir_list



def compare_local_and_remote_folder_names(remote_folder_list, local_folder_list):
	remote_folder_names = [item['name'] for item in remote_folder_list]
	local_folder_names = [item['name'] for item in local_folder_list]

	print("Remote Folder Names:")
	print(remote_folder_names)

	print("\nLocal Folder Names:")
	print(local_folder_names)
	
	if set(remote_folder_names) != set(local_folder_names):
		# folders do not contain the same subdirectory names; let's figure out what they are
		local_discrepancies = [name for name in local_folder_names if name not in remote_folder_names]
		remote_discrepancies = [name for name in remote_folder_names if name not in local_folder_names]

		print("\nUserWarning: The remote and local folders do not contain the same sub directories!")
		print("\tLocal discrepancies: {}".format(local_discrepancies))
		print("\tRemote discrepancies: {}".format(remote_discrepancies))
		if raw_input("Do you want to continue? (yes/no): ").lower().strip() not in ("yes", "Yes"):
			print("Good choice. Exiting.")
			sys.exit(1)
	
	return True

def delete_specific_subfolder_for_each_school(service, remote_folder_id, sub_folder_name):

	try:
		response = service.files().get(fileId=remote_folder_id, fields="name").execute()
	except errors.HttpError, error:
		print('An error occurred: {}'.format(error))

	remote_file_name = response['name']

	print("\nUserWarning: You are about to delete each subdirectory called '{}' for each school folder in the remote folder '{}'.".format(sub_folder_name, remote_file_name))
	if raw_input("Do you want to continue? (yes/no): ").lower().strip() not in ("yes", "Yes"):
		print("Good choice. Exiting.")
		sys.exit(1)


	# query = "'{}' in parents and mimeType = 'application/vnd.google-apps.folder' and trashed = FALSE".format(remote_folder_id)

	# try:
	# 	response = service.files().list(q=query,
	# 		spaces='drive',
	# 		fields='files(id, name)').execute()
	# except errors.HttpError, error:
	# 	print('An error occurred: {}'.format(error))

	# return response['files']

	remote_metadata = get_metadata_in_remote_folder(service=service, remote_folder_id=remote_folder_id)
	remote_folder_ids = [item['id'] for item in remote_metadata]
	# print(remote_folder_ids)

	# build query so we can do this in a single request
	padded_remote_folder_ids = ["'" + folder_id + "' in parents" for folder_id in remote_folder_ids]
	query = "(" + " or ".join(padded_remote_folder_ids) + ") and mimeType = 'application/vnd.google-apps.folder' and name = '{}' and trashed = FALSE".format(sub_folder_name)
	# print(query)

	try:
		response = service.files().list(q=query,
			spaces='drive',
			fields='files(id, name)').execute()
	except errors.HttpError, error:
		print('An error occurred: {}'.format(error))

	folder_ids_to_trash = [item['id'] for item in response['files']]
	# print(folder_ids_to_trash)

	# create callback function for batch response
	def callback(request_id, response, exception):
		if exception:
			# Handle error
			print(exception)

	# create a batch request
	batch = service.new_batch_http_request(callback=callback)

	for file in response['files']:
		batch.add(service.files().delete(fileId=file["id"]))

	print("Batch deleting files...", end=" ")
	batch.execute()
	print("Done!", end="\n\n")

def delete_all_remote_documents(service, remote_folder_id):

	try:
		response = service.files().get(fileId=remote_folder_id, fields="name").execute()
	except errors.HttpError, error:
		print('An error occurred: {}'.format(error))

	remote_file_name = response['name']

	print("\nUserWarning: You are about to delete ALL files in each subdirectory in the remote folder '{}'.".format(remote_file_name))
	if raw_input("Do you want to continue? (yes/no): ").lower().strip() not in ("yes", "Yes"):
		print("Good choice. Exiting.")
		sys.exit(1)

	remote_metadata = get_metadata_in_remote_folder(service=service, remote_folder_id=remote_folder_id)
	remote_folder_ids = [item['id'] for item in remote_metadata]
	# print(remote_folder_ids)

	# build query so we can do this in a single request
	padded_remote_folder_ids = ["'" + folder_id + "' in parents" for folder_id in remote_folder_ids]
	query = "(" + " or ".join(padded_remote_folder_ids) + ") and trashed = FALSE"
	# print(query)

	try:
		response = service.files().list(q=query,
			spaces='drive',
			fields='files(id, name)').execute()
	except errors.HttpError, error:
		print('An error occurred: {}'.format(error))

	# file_ids_to_trash = [item['id'] for item in response['files']]
	# print(file_ids_to_trash)

	# create callback function for batch response
	def callback(request_id, response, exception):
		if exception:
			# Handle error
			print(exception)

	# create a batch request
	batch = service.new_batch_http_request(callback=callback)

	for file in response['files']:
		batch.add(service.files().delete(fileId=file["id"]))

	print("Batch deleting files...", end=" ")
	batch.execute()
	print("Done!", end="\n\n")

def upload_local_folders_to_remote(service, remote_metadata, sub_folder_name, local_metadata):

	upload_list = []
	for item in local_metadata:
		subfolder_path = item["path"]
		school_name = item["name"]

		remote_school_folder_id = next((item['id'] for item in remote_metadata if item['name'] == school_name), None)
		# print(school_name, subfolder_path, remote_school_folder_id)

		# create subfolder in this school folder
		file_metadata = {
			'name': sub_folder_name,
			'mimeType': 'application/vnd.google-apps.folder',
			'parents': [remote_school_folder_id]
		}

		file = service.files().create(body=file_metadata, fields='id').execute()
		parent_folder_id = file.get('id')

		for filename in os.listdir(subfolder_path):
			subitem_path = os.path.join(subfolder_path, filename)
			if os.path.isfile(subitem_path) and filename != '.DS_Store':
				# print('\t' + subitem_path)
				upload_list.append( (subitem_path, filename, parent_folder_id) )

	print("Uploading {} files...".format(len(upload_list)))
	count = 0
	for filepath, item_name, parent_folder_id in upload_list:
		count += 1
		print("\t{}/{}  {}".format(count, len(upload_list), item_name))
		file_metadata = {
			'name': item_name,
			'parents': [parent_folder_id]
		}

		media = MediaFileUpload(filepath,
			mimetype='application/pdf',
			resumable=True)

		file = service.files().create(body=file_metadata,
			media_body=media,
			fields='id').execute()

		# print('File ID: %s' % file.get('id'))
	print('Done!')	
		
# def upload_local_files_to_remote(service, remote_metadata, local_folder_path):

# 	remote_folder_mapping = {item['name'] : item['id'] for item in remote_metadata}

# 	upload_list = []

# 	for item in os.listdir(local_folder_path):
# 		# check if this is a directory or file
# 		item_path = os.path.join(local_folder_path, item)
# 		if os.path.isdir(item_path):
# 			# this is a directory
# 			for subitem in os.listdir(item_path):
# 				subitem_path = os.path.join(item_path, subitem)
# 				# check if subitem is a file
# 				if os.path.isfile(subitem_path) and subitem != '.DS_Store':
# 					# print("parent: {}\tfile: {}".format(item, subitem))
# 					entry = (subitem_path, subitem, remote_folder_mapping[item])
# 					upload_list.append(entry)


# 	print("Uploading {} files...".format(len(upload_list)))
# 	count = 0
# 	for filepath, item_name, parent_folder_id in upload_list:
# 		count += 1
# 		print("\t{}/{}  {}".format(count, len(upload_list), item_name))
# 		file_metadata = {
# 			'name': item_name,
# 			'parents': [parent_folder_id]
# 		}

# 		media = MediaFileUpload(filepath,
# 			mimetype='application/pdf',
# 			resumable=True)

# 		file = service.files().create(body=file_metadata,
# 			media_body=media,
# 			fields='id').execute()

# 		# print('File ID: %s' % file.get('id'))
# 	print('Done!')	


def main():

	# choose local directory and get subfolder names and paths
	local_directory_path = choose_local_directory()
	# local_directory_path = LOCAL_FOLDER_PATH
	local_dir_name, local_metadata = get_local_folder_metadata(local_directory_path)

	# get remote folder data
	service = setup_drive_v3_api()
	remote_metadata = get_metadata_in_remote_folder(service=service, remote_folder_id=DRIVE_FOLDER_ID)
	# print(remote_metadata)

	# make sure the folder names match up
	compare_local_and_remote_folder_names(remote_metadata, local_metadata)

	delete_specific_subfolder_for_each_school(service=service, remote_folder_id=DRIVE_FOLDER_ID, sub_folder_name=local_dir_name)

	upload_local_folders_to_remote(service=service, remote_metadata=remote_metadata, sub_folder_name=local_dir_name, local_metadata=local_metadata)

if __name__ == '__main__':
	main()




